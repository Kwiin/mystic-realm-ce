# The Mystic Realm Community Edition repository

To build a pk3 from the current source, run create_pk3.bat, which will package the contents of /src into a valid pk3 addon.
It will also search through /src to fix up any files with the .lmp extension, which may take some time if there's a lot of them.
