--FixedLerp
local function FixedLerp(a, b, w)
    return FixedMul((FRACUNIT - w), a) + FixedMul(w, b)
end

addHook("ThinkFrame", function()
	for p in players.iterate
		if (p.charability2 == CA2_SPINDASH or p.mo.state == S_PLAY_ROLL or p.mo.state == S_PLAY_SPINDASH) and p.spinitem
			p.spinitem = 0
		end
	end
end)

addHook("PostThinkFrame", function()
	for player in players.iterate()
		local mo = player.mo
		if not (mo and mo.valid) continue end
		if (player == nil) or (player.playerstate != PST_LIVE) continue end
	
		if (mo.previousx == nil) or (mo.previousy == nil) or (mo.previousz == nil)
			mo.previousx = mo.x
			mo.previousy = mo.y
			mo.previousz = mo.z
		end
		
		if (player.speed > 15*FRACUNIT) and (player.pflags & PF_SPINNING) and not (player.pflags & PF_JUMPED)
			for i = 0, 9
				local percent = FRACUNIT * (i * 10) / 100
				local trail = P_SpawnMobj(mo.x, mo.y, mo.z, MT_THOK)
				local tx = FixedLerp(mo.x,mo.previousx,percent)
				local ty = FixedLerp(mo.y,mo.previousy,percent)
				local tz = FixedLerp(mo.z + 3*FRACUNIT,mo.previousz + 4*FRACUNIT,percent)
				P_TeleportMove(trail, tx, ty, tz - 7 * FRACUNIT)
				if (player.mo.skin == "shadow") and player.mo.color == SKINCOLOR_BLACK
					trail.color = SKINCOLOR_YELLOW
				else
					trail.color = mo.color
				end
				trail.fuse = 13
				trail.state = S_THOK
				trail.scalespeed = FRACUNIT/12
				trail.scale = (FRACUNIT * 5/6) - (i * FRACUNIT/120)
				trail.destscale = 0
				trail.frame = TR_TRANS70|A
			end
		end

		mo.previousx = mo.x
		mo.previousy = mo.y
		mo.previousz = mo.z
	end
end)
